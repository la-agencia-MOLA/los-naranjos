//funciones
function scrollWhatsapp(){
  let posScroll = $(window).scrollTop();
  let anchoPant = $(window).width();
  if (anchoPant > 1279) {
    if (posScroll > 8500) {
        $('.flotante-whatsapp').css('display','none');
    }else{
      $('.flotante-whatsapp').css('display','block');
    }
  }else if (anchoPant >= 1024){
    if (posScroll > 7850) {
        $('.flotante-whatsapp').css('display','none');
    }else{
      $('.flotante-whatsapp').css('display','block');
    }
  }else if (anchoPant >= 768){
    if (posScroll > 7580) {
        $('.flotante-whatsapp').css('display','none');
    }else{
      $('.flotante-whatsapp').css('display','block');
    }
  }
  else if (anchoPant >= 375){
    if (posScroll > 8850) {
        $('.flotante-whatsapp').css('display','none');
    }else{
      $('.flotante-whatsapp').css('display','block');
    }
  }
}
function menuMobile(){
  if ($('.menu').hasClass('open')) {
    $('.menu').removeClass('open');
    $('.hamburguer-icon .icon').removeClass('animate-icon');
  }else{
    $('.menu').addClass('open');
    $('.hamburguer-icon .icon').addClass('animate-icon');
  }
}
function actionMenuMobile(){
  let anchoDisp = $(window).width();
  if (anchoDisp <= 1279) {
    $('.menu').removeClass('open');
    $('.hamburguer-icon .icon').removeClass('animate-icon');
  }
}


$(document).ready(function(){
  $('.carousel-proyecto').owlCarousel({
  loop:true,
  margin:10,
  nav:true,
  dots:false,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
    }
  })
  $('.carousel-casa').owlCarousel({
  loop:true,
  margin:10,
  nav:false,
  dots:false,
  autoplay:true,
  responsive:{
      0:{
          items:1
      },
      768:{
          items:2
      },
      1280:{
          items:2
      }
    }
  })
  $('.carousel-mobile').owlCarousel({
  loop:true,
  margin:10,
  nav:true,
  dots:false,
  responsive:{
      0:{
          items:1
      },
      768:{
          items:1
      },
      1280:{
          items:1
      }
    }
  })

  $(window).scroll(scrollWhatsapp);

  $('nav .hamburguer-icon .icon img').on('click', function(){
    menuMobile();
  });
  $('nav .menu li a').on('click', function(){
    actionMenuMobile();
  });
})
